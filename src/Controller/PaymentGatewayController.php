<?php

namespace Drupal\commerce_klarna_checkout_external_payment\Controller;

use Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway\KlarnaCheckoutInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class PaymentGatewayController.
 *
 * @package Drupal\commerce_klarna_checkout_external_payment\Controller
 */
class PaymentGatewayController extends ControllerBase {

  /**
   * Switch payment gateway to the external payment gateway provided by Klarna.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   Commerce order.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   Payment gateway.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Reload page.
   */
  public function switchPaymentGateway(OrderInterface $commerce_order, PaymentGatewayInterface $commerce_payment_gateway) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $current_payment_gateway */
    $current_payment_gateway = $commerce_order->get('payment_gateway')->entity;

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager()->getStorage('commerce_payment_gateway');
    $payment_gateways = $payment_gateway_storage->loadMultipleForOrder($commerce_order);

    // Only allow switching if:
    // - order is locked,
    // - current payment gateway is klarna,
    // - new payment gateway is allowed for given order.
    // Additional checks, such as order ownership, are done in access callback.
    if ($commerce_order->isLocked() && ($current_payment_gateway->getPlugin() instanceof KlarnaCheckoutInterface) && in_array($commerce_payment_gateway->id(), array_keys($payment_gateways))) {
      $commerce_order->set('payment_gateway', $commerce_payment_gateway);
      // This should not be necessary as order is locked. But it doesn't hurt.
      $commerce_order->setRefreshState(OrderInterface::REFRESH_SKIP);
      $commerce_order->save();
    }
    else {
      $this
        ->getLogger('commerce_klarna_checkout_payment_gateway')
        ->warning('An attempt to change payment gateway for order @order_id from @current to @next', [
          '@order_id' => $commerce_order->id(),
          '@current' => $current_payment_gateway->id(),
          '@next' => $commerce_payment_gateway->id(),
        ]);
    }

    // Reload.
    return $this->redirect('commerce_checkout.form', ['commerce_order' => $commerce_order->id(), 'step' => 'payment']);
  }

}
