<?php

namespace Drupal\commerce_klarna_checkout_external_payment\EventSubscriber;

use Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents;
use Drupal\commerce_klarna_checkout\Event\OrderRequestEvent;
use Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway\KlarnaCheckoutInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * External payment event subscriber.
 */
class ExternalPaymentSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The list of available external providers.
   *
   * @var array
   */
  static public $providers = [
    'Achteraf betalen' => '',
    'Alipay' => '',
    'Amazon' => '',
    'Amazon Pay' => '',
    'Amex' => '',
    'Apple Pay' => '',
    'Bancontact' => '',
    'Bank Transfer' => '',
    'Bankoverførsel' => '',
    'Banköverföring' => '',
    'Barzahlen' => '',
    'Barzahlung Bei Abholung' => '',
    'Bedriftsfaktura' => '',
    'Betal i butikk' => '',
    'Betal på verkstedet' => '',
    'Betala i butik' => '',
    'Betala på plats' => '',
    'Betala på station' => '',
    'Betalning på betjäningsstället' => '',
    'bitcoin' => '',
    'Bonifico Bancario' => '',
    'Bussiennakko' => '',
    'Business Invoice' => '',
    'Card via PayPal' => '',
    'Carte Bancaires' => '',
    'Carte Bleue' => '',
    'Cash on Delivery' => '',
    'Cash on Hand' => '',
    'ClickandBuy' => '',
    'CoinPayments' => '',
    'Coop Matkonto' => '',
    'Dankort' => '',
    'Consors Finanz' => '',
    'Delbetal i ditt eget tempo' => '',
    'Delbetalning' => '',
    'Divide.Connect' => '',
    'EAN Fakturering' => '',
    'EU- standaard bankoverschrijving' => '',
    'EU-Standard Bank Transfer' => '',
    'Ennakkomaksu' => '',
    'Faktura' => '',
    'Faktura 14 dagar' => '',
    'Faktura 14 dage' => '',
    'Fast delbetaling' => '',
    'Forskudd' => '',
    'Förskottsbetalning' => '',
    'Gavekort' => '',
    'Giropay' => '',
    'Google Wallet' => '',
    'iDeal' => '',
    'Konto' => '',
    'Kort' => '',
    'Kortti' => '',
    'Kreditkarte' => '',
    'Lasku 14 päivää' => '',
    'Lastschrift' => '',
    'M-Cash' => '',
    'Maksu noudon yhteydessä' => '',
    'Maksu liikkeessä' => '',
    'Maksu palvelupisteellä' => '',
    'MobilePay' => '',
    'Multibanco' => '',
    'Nachnahme' => '',
    'Pagamento Alla Consegna' => '',
    'Partner' => '',
    'Pay at office' => '',
    'Pay at station' => '',
    'Pay by Card or PayPal' => '',
    'Pay in-store' => '',
    'Paydirekt' => '',
    'PayPal' => '',
    'PayPalExpress' => '',
    'Postförskott' => '',
    'Postiennakko' => '',
    'Postoppkrav' => '',
    'SOFORT Überweisung' => '',
    'Strix' => '',
    'Swish' => '',
    'Transferencia Bancaria' => '',
    'Verkkolasku' => '',
    'Verkkomaksu' => '',
    'Vipps' => 'vipps.png',
    'Virement bancaire' => '',
    'Vorkasse' => '',
    'Vorkasse Banküberweisung' => '',
    'Wire Transfer' => '',
    'Zahlung bei Abholung' => '',
    '銀行振込' => '',
  ];

  /**
   * ExpressSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Filter Payment Gateways.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $payment_gateways
   *   Available payment gateways.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface[]
   *   List of payment gateways.
   */
  protected function filterExternalGateways(array $payment_gateways) {
    return array_filter($payment_gateways, function (PaymentGatewayInterface $payment_gateway) {
      return !($payment_gateway->getPlugin() instanceof KlarnaCheckoutInterface);
    });
  }

  /**
   * Add shipping callbacks.
   *
   * @param \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent $event
   *   Order Request Event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function externalPaymentGateways(OrderRequestEvent $event) {
    $data = $event->getRequestData();
    $order_id = $data['merchant_reference2'];
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $paymentGatewayStorage */
    $paymentGatewayStorage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $paymentGateways = $paymentGatewayStorage->loadMultipleForOrder($order);

    foreach ($paymentGateways as $paymentGateway) {
      try {
        $logo = self::getLogoFilename($paymentGateway->label());
        $data['external_payment_methods'][] = $test = [
          'name' => $paymentGateway->label(),
          'redirect_url' => Url::fromRoute('commerce_klarna_checkout_external_payment.switch_payment_gateway', [
            'commerce_order' => $order->id(),
            'commerce_payment_gateway' => $paymentGateway->id(),
          ])
            ->setAbsolute()
            ->toString(),
          'image_url' => $logo ? Url::fromUserInput('/' . drupal_get_path('module', 'commerce_klarna_checkout_external_payment') . '/assets/' . $logo)->setAbsolute()->toString() : '',
          'fee' => 0,
          'description' => self::getDescription($paymentGateway->label()),
          // @todo: List should be configurable per payment gateway.
          'countries' => ['NO'],
          'label' => 'continue',
        ];
      }
      catch (\InvalidArgumentException $exception) {
        // This provider is not allowed by Klarna.
        continue;
      }
    }

    $event->setRequestData($data);
  }

  /**
   * Get logo filename for the provider.
   *
   * @param string $name
   *   Provider's name.
   *
   * @return string
   *   Logo filename.
   */
  public static function getLogoFilename($name) {
    if (!isset(self::$providers[$name])) {
      throw new \InvalidArgumentException('This payment provider is not allowed.');
    }
    return self::$providers[$name];
  }

  /**
   * Get description for the provider.
   *
   * @param string $name
   *   Prpvider's name.
   *
   * @return string
   *   The description.
   */
  public static function getDescription($name) : string {
    switch ($name) {
      case 'Vipps':
        return (string) t('Remember: Vipps is always without fees when paying businesses.');

      default:
        return (string) t('This is an external payment gateway.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KlarnaCheckoutEvents::CREATE_ORDER_REQUEST => ['externalPaymentGateways'],
      KlarnaCheckoutEvents::UPDATE_ORDER_REQUEST => ['externalPaymentGateways'],
    ];
  }

}
